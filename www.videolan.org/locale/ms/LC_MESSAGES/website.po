# Malay translation
# Copyright (C) 2021 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# abuyop <abuyop@gmail.com>, 2013-2017,2021
# Mahrazi Mohd Kamal <mahrazi@gmail.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2021-01-28 06:58+0100\n"
"Last-Translator: abuyop <abuyop@gmail.com>, 2021\n"
"Language-Team: Malay (http://www.transifex.com/yaron/vlc-trans/language/"
"ms/)\n"
"Language: ms\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: include/header.php:289
msgid "a project and a"
msgstr "sebuah projek dan"

#: include/header.php:289
msgid "non-profit organization"
msgstr "organisasi yang tidak berasaskan keuntungan"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Rakan kongsi"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "Pasukan &amp; Organisasi"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "Perkhidmatan Rundingan &amp; Rakan Kongsi"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Acara"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Perundangan"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Pusat akhbar"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Hubungi kami"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Muat turun"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Fitur"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "Suai"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Dapatkan Barangan"

#: include/menus.php:51
msgid "Projects"
msgstr "Projek"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Semua Projek"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Sumbangan"

#: include/menus.php:77
msgid "Getting started"
msgstr "Membiasakan diri"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "Derma"

#: include/menus.php:79
msgid "Report a bug"
msgstr "Laporkan pepijat"

#: include/menus.php:83
msgid "Support"
msgstr "Sokongan"

#: include/footer.php:33
msgid "Skins"
msgstr "Kulit"

#: include/footer.php:34
msgid "Extensions"
msgstr "Sambungan"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Tangkap layar"

#: include/footer.php:61
msgid "Community"
msgstr "Komuniti"

#: include/footer.php:64
msgid "Forums"
msgstr "Forum"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Senarai-Mel"

#: include/footer.php:66
msgid "FAQ"
msgstr "FAQ"

#: include/footer.php:67
msgid "Donate money"
msgstr "Derma wang"

#: include/footer.php:68
msgid "Donate time"
msgstr "Derma masa"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Projek dan Organisasi"

#: include/footer.php:76
msgid "Team"
msgstr "Pasukan"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Cermin"

#: include/footer.php:83
msgid "Security center"
msgstr "Pusat sekuriti"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Turut Serta"

#: include/footer.php:85
msgid "News"
msgstr "Berita"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "Muat turun VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Sistem Lain"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "muat turun setakat ini"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC merupakan pemain multimedia dan bingkai kerja platform-silang bersumber "
"terbuka dan percuma yang dapat mainkan kebanyakan fail multimedia seperti "
"DVD, CD Audio, VCD dan pelbagai protokol penstriman."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC merupakan pemain multimedia dan bingkai kerja platform-silang bersumber "
"terbuka yang dapat mainkan kebanyakan fail multimedia dan pelbagai protokol "
"penstriman."

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr "VLC: Laman rasmi - Penyelesaian multimedia bebas untuk semua OS!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Lain-lain projek daripada VideoLAN"

#: index.php:30
msgid "For Everyone"
msgstr "Untuk Semua Orang"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC merupakan sebuah pemain media berkuasa yang dapat memainkan kebanyakan "
"kodeks media dan format video di luar sana."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"Pencipta Filem VideoLAN ialah sebuah perisian bukan-linear untuk penciptaan "
"video."

#: index.php:62
msgid "For Professionals"
msgstr "Untuk Para Profesional"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast ialah sebuah aplikasi demux atau penyahmultipleks MPEG-2/TS dan "
"penstriman yang berkuasa dan ringkas. "

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicast ialah satu set alatan yang direka untuk memanipulasi strim "
"multikas & TS secara efisyen dan ringkas."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 ialah sebuah aplikasi bebas untuk mengkod strim-strim video dalam "
"format H.264/MPEG-4 AVC."

#: index.php:104
msgid "For Developers"
msgstr "Untuk Para Pembangun"

#: index.php:140
msgid "View All Projects"
msgstr "Lihat Semua Projek"

#: index.php:144
msgid "Help us out!"
msgstr "Bantulah kami!"

#: index.php:148
msgid "donate"
msgstr "derma"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN adalah organisasi tidak berasaskan keuntungan."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"Semua kos kami diperolehi dari sumbangan yang diterima daripada pengguna "
"kami. Jika anda menyukai produk VideoLAN, silalah memberi derma sebagai "
"tanda sokongan terhadap kami."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Ketahui Lagi"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN ialah perisian sumber-terbuka."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"Ini bermakna jika anda memiliki kemahiran dan keinginan untuk memperbaiki "
"salah satu daripada produk kami, sumbangan ikhlas anda dialu-alukan"

#: index.php:187
msgid "Spread the Word"
msgstr "Sebar Luaskan"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Kami yakin VideoLAN merupakan perisian video terbaik yang ada pada harga "
"terbaik: iaitu percuma. Jika anda bersetuju, bantu kami menyebarluaskan "
"perihal perisian kami ini."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Berita &amp; Kemas Kini"

#: index.php:218
msgid "More News"
msgstr "Lagi Banyak Berita"

#: index.php:222
msgid "Development Blogs"
msgstr "Blog Pembangunan"

#: index.php:251
msgid "Social media"
msgstr "Media sosial"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr "Muat turun rasmi pemain media VLC, pemain Sumber Terbuka yang terbaik"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "Dapatkan VLC untuk"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "Ringkas, pantas dan berkuasa"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "Main segalanya"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "Fail, Cakera, Kamera Sesawang, Peranti dan Strim."

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr "Main kebanyakan kodeks tanpa memerlukan pek-pek kodeks"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "Boleh dijalankan pada semua platform"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "Sepenuhnya Percuma"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr "tanpa perisian intip, tanpa iklan dan tidak menjejak pengguna."

#: vlc/index.php:47
msgid "learn more"
msgstr "ketahui lagi"

#: vlc/index.php:66
msgid "Add"
msgstr "Tambah"

#: vlc/index.php:66
msgid "skins"
msgstr "kulit"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "Cipta kulit dengan"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "penyunting kulit VLC"

#: vlc/index.php:72
msgid "Install"
msgstr "Pasang"

#: vlc/index.php:72
msgid "extensions"
msgstr "sambungan"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Lihat semua tangkap layar"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Muat Turun Rasmi pemain media VLC"

#: vlc/index.php:146
msgid "Sources"
msgstr "Sumber"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Anda boleh juga mendapatkan secara terus"

#: vlc/index.php:148
msgid "source code"
msgstr "kod sumber secara terus"

#~ msgid "A project and a"
#~ msgstr "Sebuah projek dan sebuah"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "terdiri daripada sukarela, membangunkan dan mempromosikan penyelesaian "
#~ "multimedia bebas, sumber terbuka. "

#~ msgid "why?"
#~ msgstr "mengapa?"

#~ msgid "Home"
#~ msgstr "Rumah"

#~ msgid "Support center"
#~ msgstr "Pusat sokongan"

#~ msgid "Dev' Zone"
#~ msgstr "Zon Pembangun"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "Pemain media mudah, pantas dan berkuasa."

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr "Main segalanya: Fail, Cakera, Kamera Sesawang, Peranti dan Strim."

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr "Main kebanyakan kodeks tanpa memerlukan pek kodeks:"

#~ msgid "Runs on all platforms:"
#~ msgstr "Boleh dijalankan pada semua platform:"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr ""
#~ "Sepenuhnya Percuma, tanpa perisian intip, tanpa iklan dan tidak menjejak "
#~ "pengguna."

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Boleh melakukan penukaran media dan striming."

#~ msgid "Discover all features"
#~ msgstr "Jelajahi semua fitur"
