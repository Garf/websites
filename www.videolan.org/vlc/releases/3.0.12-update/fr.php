            <center><h1 class='bigtitle' style="padding-bottom: 3px;"><em>Problème dans le système de mises à jour automatique</em> de VLC <b>3.0.12</b> et <b>3.0.13</b></h1>
            <div style="padding-top: 0px; padding-bottom: 10px; color: grey;">Un bug dans le système de mise à jour empêche les utilisateurs et utilisatrices sous Windows de mettre à jour VLC automatiquement.</div>
            </center>

        <div class="container">

    <center><h2>Cette page concerne uniquement les utilisateurs et utilisatrices de Windows</h2></center>

<h3>Version courte:</h3>
<ul>
<li>- Les versions 3.0.12 et 3.0.13 ne peuvent <b>pas</b> se mettre à jour automatiquement et <b>nécessitent</b> une intervention manuelle</li>
<li>- Les versions 3.0.11 et antérieures se mettront à jour automatiquement vers la version 3.0.14</li>
</ul>
<br/>

<h3>Description:</h3>
Cette page se destine uniquement aux utilisateurs et utilisatrices de VLC 3.0.12 et 3.0.13.<br/>
À cause d'une erreur dans le code du système de mise à jour automatique, la nouvelle version sera téléchargée, son intégrité vérifiée, mais elle ne sera pas installée. Ceci est un problème grave, et nous aimerions vous présenter nos excuses pour cela.<br/><br/>

<h3>Instructions:</h3>
Afin de mettre à jour vers la version 3.0.14, vous devrez vous rendre sur <a href="https://www.videolan.org/vlc">https://www.videolan.org/vlc</a> pour télécharger et installer manuellement la nouvelle version de VLC.<br/>
Vous pouvez trouver des instructions détaillées <a href="https://docs.videolan.me/vlc-user/3.0/en/gettingstarted/setup/windows.html">ici</a>.<br/><br/>

Si vous avez déjà téléchargé la mise à jour via VLC et que vous ne souhaitez pas re-télécharger l'installeur, vous pouvez l'exécuter manuellement en ouvrant l'explorateur de fichier windows (Touche Windows + E, ou bien en cliquant sur l'icone de l'explorateur de fichiers) puis en rentrant <em>%TEMP%</em> comme destination.<br/>
Vous trouverez l'installeur dans ce dossier, il portera le nom «vlc-3.0.14-win32.exe» ou «vlc-3.0.14-win64.exe» respectivement, selon si vous utilisez un version 32bits ou 64bits de Windows.<br/>
<br/>
<?php image("screenshots/3.0.12-update.jpg" , "3.0.12 update screen", "center-block img-responsive"); ?>
<br/>
<br/>

<h3>Post mortem:</h3>
Le 10 Mai 2021, l'équipe VideoLAN a publié la version 3.0.13 de VLC, et a activé les mises à jours automatiques vers cette nouvelle version.<br/>
Ceci se déroule généralement sans encombre et il suffit d'accepter la proposition de VLC de se mettre à jour, la mise à jour se télécharge, s'installer, et c'est tout.<br/>
Cependant, et malheureusement, cette fois si le choses sont différentes, et quelques étapes supplémentaires seront nécessaires.<br/>
Le problème a été introduit dans la version 3.0.12, mais il n'est devenu apparent que lorsque nous avons lancé la version 3.0.13.<br/>
Bien que le problème soit reglé dans la version 3.0.14, nous ne pouvons pas déployer cette correction nous même pour les gens ayant déjà installé la version 3.0.12.<br/>

<br/>
<a href="https://code.videolan.org/videolan/vlc-3.0/-/commit/83d8e7efaa4f7dc23b07c47c59431e1f6df57da5">
Le commit ayant introduit le problème</a><br/>
<a href="https://code.videolan.org/videolan/vlc-3.0/-/commit/d456994213b98933664bd6aee2e8f09d5dea5628">Le commit ayant corrigé le problème</a><br/>


